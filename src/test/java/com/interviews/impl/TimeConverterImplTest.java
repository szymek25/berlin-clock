package com.interviews.impl;

import com.interviews.constants.ApplicationConstants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Map;

import static org.junit.Assert.*;


public class TimeConverterImplTest {

    protected TimeConverterImpl timeConverter;

    @Before
    public void setUp() throws Exception {
        timeConverter = new TimeConverterImpl();
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldSplitInputTime() {
        final Map<String, Integer> splittedInput = timeConverter.splitInput("11:25:13");
        assertEquals(11, splittedInput.get(ApplicationConstants.HOURS).intValue());
        assertEquals(25, splittedInput.get(ApplicationConstants.MINUTES).intValue());
        assertEquals(13, splittedInput.get(ApplicationConstants.SECONDS).intValue());
    }

    @Test
    public void shouldThrowExceptionIfWrongInput() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(ApplicationConstants.WRONG_TIME_FORMAT);
        timeConverter.validateInput("wrongTestTime");
    }

    @Test
    public void shouldThrowExceptionIfHoursOutOfBound() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(ApplicationConstants.HOURS_OUT_OF_BOUND);
        timeConverter.validateInput("26:00:01");
    }

    @Test
    public void shouldThrowExceptionIfMinutesOutOfBound() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(ApplicationConstants.MINUTES_OUT_OF_BOUND);
        timeConverter.validateInput("21:71:01");
    }

    @Test
    public void shouldThrowExceptionIfSecondsOutOfBound() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(ApplicationConstants.SECONDS_OUT_OF_BOUND);
        timeConverter.validateInput("22:01:99");

    }

    @Test
    public void shouldReturnProperLampValueForSeconds() {
        String oddValue = timeConverter.getLampForSeconds(11);
        String evenValue = timeConverter.getLampForSeconds(24);

        assertEquals(ApplicationConstants.OFF_LAMP, oddValue);
        assertEquals(ApplicationConstants.YELLOW_LAMP, evenValue);
    }

    @Test
    public void shouldReturnProperLampValuesForHours() {
        assertEquals("OOOO RROO", timeConverter.getLampsForHours(2));
        assertEquals("OOOO OOOO", timeConverter.getLampsForHours(0));
        assertEquals("RRRO ROOO", timeConverter.getLampsForHours(16));
        assertEquals("RROO OOOO", timeConverter.getLampsForHours(10));
        assertEquals("RROO RRRO", timeConverter.getLampsForHours(13));
        assertEquals("RRRR RRRO", timeConverter.getLampsForHours(23));
        assertEquals("RRRR RRRR", timeConverter.getLampsForHours(24));
    }

    @Test
    public void shouldReturnProperLampValuesForMinutes() {
        assertEquals("YYROOOOOOOO YYOO", timeConverter.getLampsForMinutes(17));
        assertEquals("YYRYYRYYRYY YYYY", timeConverter.getLampsForMinutes(59));
        assertEquals("OOOOOOOOOOO OOOO", timeConverter.getLampsForMinutes(0));
    }
}