package com.interviews.impl;

import com.interviews.TimeConverter;
import com.interviews.constants.ApplicationConstants;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeConverterImpl implements TimeConverter {

    private static final String timeFormat = "\\d\\d:\\d\\d:\\d\\d";
    private final static Pattern pattern = Pattern.compile(timeFormat);


    @Override
    public String convertTime(final String aTime) {
        validateInput(aTime);
        final Map<String, Integer> splitInput = splitInput(aTime);
        final String lampForSeconds = getLampForSeconds(splitInput.get(ApplicationConstants.SECONDS));
        final String lampForMinutes = getLampsForMinutes(splitInput.get(ApplicationConstants.MINUTES));
        final String lampForHours = getLampsForHours(splitInput.get(ApplicationConstants.HOURS));

        return lampForSeconds + "\r\n" + lampForHours + "\r\n" + lampForMinutes;
    }

    protected void validateInput(final String aTime) {
        final Matcher matcher = pattern.matcher(aTime);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(ApplicationConstants.WRONG_TIME_FORMAT);
        }
        String[] split = aTime.split(":");
        final Integer hours = Integer.valueOf(split[0]);
        if (hours > 24) {
            throw new IllegalArgumentException(ApplicationConstants.HOURS_OUT_OF_BOUND);
        }
        final Integer minutes = Integer.valueOf(split[1]);
        if (minutes > 59) {
            throw new IllegalArgumentException(ApplicationConstants.MINUTES_OUT_OF_BOUND);
        }

        final Integer seconds = Integer.valueOf(split[2]);
        if (seconds > 59) {
            throw new IllegalArgumentException(ApplicationConstants.SECONDS_OUT_OF_BOUND);
        }
    }

    protected Map<String, Integer> splitInput(final String aTime) {
        final Map<String, Integer> results = new HashMap<>();
        String[] split = aTime.split(":");
        results.put(ApplicationConstants.HOURS, Integer.valueOf(split[0]));
        results.put(ApplicationConstants.MINUTES, Integer.valueOf(split[1]));
        results.put(ApplicationConstants.SECONDS, Integer.valueOf(split[2]));

        return results;
    }

    protected String getLampForSeconds(final Integer seconds) {
        if (seconds % 2 == 0) {
            return ApplicationConstants.YELLOW_LAMP;
        } else {
            return ApplicationConstants.OFF_LAMP;
        }
    }

    protected String getLampsForHours(final Integer hours) {
        Integer valuesForFirstRow = hours / 5;
        Integer valuesForSecondRow = hours % 5;

        return lightUpFourLamps(valuesForFirstRow, ApplicationConstants.RED_LAMP)
                + "\r\n" + lightUpFourLamps(valuesForSecondRow, ApplicationConstants.RED_LAMP);
    }

    protected String lightUpFourLamps(final Integer lampsToEnable, final String lampColor) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            if (i < lampsToEnable) {
                builder.append(lampColor);
            } else {
                builder.append(ApplicationConstants.OFF_LAMP);
            }
        }
        return builder.toString();
    }

    protected String getLampsForMinutes(final Integer minutes) {
        final Integer valuesForFirstRow = minutes / 5;
        final Integer valuesForSecondRow = minutes % 5;

        final StringBuilder firstRow = new StringBuilder();

        for (int i = 0; i < 11; i++) {
            if (i < valuesForFirstRow) {
                if (i == 2 || i == 5 || i == 8) {
                    firstRow.append(ApplicationConstants.RED_LAMP);
                } else {
                    firstRow.append(ApplicationConstants.YELLOW_LAMP);
                }
            } else {
                firstRow.append(ApplicationConstants.OFF_LAMP);
            }
        }
        return firstRow.toString() + "\r\n" + lightUpFourLamps(valuesForSecondRow, ApplicationConstants.YELLOW_LAMP);
    }
}
