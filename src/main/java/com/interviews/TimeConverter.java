package com.interviews;

public interface TimeConverter {

    String convertTime(String aTime);

}
