package com.interviews.constants;

public class ApplicationConstants {
    public static final String SECONDS = "seconds";
    public static final String HOURS = "hours";
    public static final String MINUTES = "minutes";

    public static final String OFF_LAMP = "O";
    public static final String RED_LAMP = "R";
    public static final String YELLOW_LAMP = "Y";

    public static final String WRONG_TIME_FORMAT = "Wrong time format";
    public static final String HOURS_OUT_OF_BOUND = "Value for hours is out of bound";
    public static final String MINUTES_OUT_OF_BOUND = "Value for minutes is out of bound";
    public static final String SECONDS_OUT_OF_BOUND = "Value for seconds is out of bound";
}
