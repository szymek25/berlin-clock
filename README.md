# Berlin Clock

The Berlin Uhr (Clock) is a strange way to show the time - just [see here](http://www.staff.hs-mittweida.de/~wuenschi/data/media/bc2.gif).
On the top of the clock there is a yellow lamp that blinks on/off every two seconds. The time is calculated by adding rectangular lamps.
 
The Berlin Clock is composed of four rows of lights and a yellow lamp on the top (see picture above for reference).

* The top yellow light blinks every couple of seconds (on for even seconds, off for odd seconds).
* The top two rows represent hours. The lights on the top row represent five hours each, while the bottom row lamps represent one hour each.
* The bottom two rows represent minutes. Again, each third-row lamp represents 5 minutes, so there are 11 of them. Each bottom row lamp represents one minute.

For example, 4 pm (16 hours) is represented by 3 lamps on of the first row and 1 light on of the second row (3x5 + 1). Equally, 27 minutes is represented by 5 lights on the third row and 2 on the very bottom row (5x5 + 2).

You may notice that lamps on the third row are all yellow, apart from the 3rd, 6th and 9th lamps which show quarters of an hour. This is just a visual convenience as they still represent 5 minutes like the yellow lamps.

Here is the colour code for lamps:
Y = Yellow
R = Red
O = Off

Here are some examples:


00:00:00    Y OOOO OOOO OOOOOOOOOOO OOOO

13:17:01    O RROO RRRO YYROOOOOOOO YYOO

23:59:59    O RRRR RRRO YYRYYRYYRYY YYYY

24:00:00    Y RRRR RRRR OOOOOOOOOOO OOOO

## Brief

We created for you a number of tests for the Berlin Clock and your goal is to provide an implementation that will make them pass. Please make sure implementation is well documented by in git commit comment messages - you should describe both functional changes as well as refactorings.

## Coding Task
 - create a local Git repository BEFORE you start
 - make commits as you go so we can see thinking process - not to many (e.g. every code change) and not to few (e.g. many change sin 20 files)
 - best practice is to commit every functional change, every new test that works, every refactoring
 - always split (also in commits) code rafactoring from functional changes
 - ensure to commit all changes before submission
 - think of your implementation as it was production ready code
 - once ready, please zip project directory and send back to us

## Our values
 - simple and elegant code that reads like a narrative
 - good code coverate and comprehensive tests that acts as living documentation
 - thinking about the code more than the writing of the code (quality over quantity)
 - transparency and feedback to support continuous learning
 - challenging boundaries where necessary

## Some hints
Are you new to Gradle? Spend 10 minutes reading (a quick start guide)[https://docs.gradle.org/current/userguide/getting_started.html]. For convenience
in this project you can find a Gradle Wrapper (`gradlew`) that should download everything you need. IntelliJ and Eclipse IDEs support Gradle projects.

JBehave testing framework is used just to implement acceptance tests, but you can write own simple unit tests in JUnit.
